﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieWorker.Models
{
    public class CinemaMovieInfo
    {
        public string StrText { get; set; }
        public string StrValue { get; set; }
    }
}
