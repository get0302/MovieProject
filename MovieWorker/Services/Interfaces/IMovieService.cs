﻿using MovieWorker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieWorker.Services
{
    public interface IMovieService
    {
        string ClimbWeb();

        Task GenerateMovieDataAsync();

        Task<ICollection<CinemaMovieInfo>> GetCinemasAsync();

        Task<ICollection<CinemaMovieInfo>> GetCinemaMoviesAsync(string cinema);

        Task<ICollection<CinemaMovieInfo>> GetCinemaMovieDatesAsync(string cinema, string movie);

        Task<ICollection<CinemaMovieInfo>> GetCinemaMovieTimesAsync(string cinema, string movie, string date);
    }
}
