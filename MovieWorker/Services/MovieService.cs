﻿using AngleSharp;
using AngleSharp.Dom;
using MovieWorker.Commons;
using MovieWorker.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieWorker.Services
{
    public class MovieService : IMovieService
    {
        public IMovieService _this;
        public MovieService()
        {
            _this = this;
        }

        /// <summary>
        /// 爬蟲 廢棄
        /// </summary>
        /// <returns></returns>
        string IMovieService.ClimbWeb()
        {
            // 建立 Browser 的配置
            var config = Configuration.Default
                .WithDefaultLoader()
                .WithDefaultCookies();

            // 根據配置建立出我們的 Browser 
            var browser = BrowsingContext.New(config);

            // 這邊用的型別是 AngleSharp 提供的 AngleSharp.Dom.Url
            var url = new Url("https://www.vscinemas.com.tw/vsweb/");

            // 使用 OpenAsync 來打開網頁抓回內容
            var document = browser.OpenAsync(url).Result;
            return document
                .QuerySelectorAll("select")
                .Select(body => body.InnerHtml)
                .ToString();
        }

        async Task IMovieService.GenerateMovieDataAsync()
        {
            var cinemas = await _this.GetCinemasAsync();

            foreach (var cinema in cinemas)
            {
                var movies = await _this.GetCinemaMoviesAsync(cinema.StrValue);

                foreach (var movie in movies)
                {
                    var dates = await _this.GetCinemaMovieDatesAsync(cinema.StrValue, movie.StrValue);

                    foreach (var date in dates)
                    {
                        var times = await _this.GetCinemaMovieTimesAsync(cinema.StrValue,movie.StrValue,date.StrValue);


                    }
                }
            }
        }

        Task<ICollection<CinemaMovieInfo>> IMovieService.GetCinemasAsync()
        {
            return HttpHelper.HttpGetAsync<ICollection<CinemaMovieInfo>>($"https://www.vscinemas.com.tw/vsweb/api/GetLstDicCinema");
        }

        Task<ICollection<CinemaMovieInfo>> IMovieService.GetCinemaMoviesAsync(string cinema)
        {
            return HttpHelper.HttpGetAsync<ICollection<CinemaMovieInfo>>($@"https://www.vscinemas.com.tw/vsweb/api/GetLstDicMovie?cinema={cinema}");
        }

        Task<ICollection<CinemaMovieInfo>> IMovieService.GetCinemaMovieDatesAsync(string cinema, string movie)
        {
            return HttpHelper.HttpGetAsync<ICollection<CinemaMovieInfo>>($@"https://www.vscinemas.com.tw/vsweb/api/GetLstDicDate?cinema={cinema}&movie={movie}");
        }

        Task<ICollection<CinemaMovieInfo>> IMovieService.GetCinemaMovieTimesAsync(string cinema, string movie, string date)
        {
            return HttpHelper.HttpGetAsync<ICollection<CinemaMovieInfo>>($@"https://www.vscinemas.com.tw/vsweb/api/GetLstDicSession?cinema={cinema}&movie={movie}&date={date}");
        }
    }
}
